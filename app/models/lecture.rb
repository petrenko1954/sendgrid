class Lecture < ActiveRecord::Base
 belongs_to :user
  validates :user_id, presence: true
  validates :lectname, presence: true, length: { maximum: 140 }
end
